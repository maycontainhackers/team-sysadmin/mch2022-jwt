<?php /** @noinspection PhpPropertyOnlyWrittenInspection */

const JWT_GLUE = '.';

const JWT_ALGO_NONE = 'none';
const JWT_ALGO_RS256 = 'RS256';

const JWT_RS_ALGO_MAP = [
    JWT_ALGO_RS256 => OPENSSL_ALGO_SHA256,
];

class JWT {
    private array $data;
    private string $token;
    private string $issuer_pubkey;
    private string $issuer_algorithm;

    private function __construct($data, $token, $issuer_pubkey, $issuer_algorithm)
    {
        $this->data = $data;
        $this->token = $token;
        $this->issuer_pubkey = $issuer_pubkey;
        $this->issuer_algorithm = $issuer_algorithm;
    }

    public static function fromToken(string $token, string $issuer_pubkey, string $issuer_algorithm) : ?self {
        if (!$token || !$issuer_pubkey || !$issuer_algorithm) {
            throw new InvalidArgumentException("One or more arguments are missing");
        }

        $token_validb64 = strtr($token, '-_', '+/');
        $segments = explode(JWT_GLUE, $token_validb64);

        if (
            $issuer_algorithm === JWT_ALGO_NONE && count($segments) !== 2 ||
            $issuer_algorithm !== JWT_ALGO_NONE && count($segments) !== 3
        ) {
            return null;
        }

        if ($issuer_algorithm !== JWT_ALGO_NONE) {
            $signature = base64_decode($segments[2], true);
            if (!$signature) {
                return null;
            }
        }

        $header = base64_decode($segments[0], true);
        if (!$header) {
            return null;
        }

        $header = json_decode($header, true, 2);
        if (
            !$header ||
            !isset($header['typ']) || $header['typ'] !== 'JWT' ||
            !isset($header['alg']) || $header['alg'] !== $issuer_algorithm
        ) {
            return null;
        }

        if (array_key_exists($issuer_algorithm, JWT_RS_ALGO_MAP)) {
            $pubkey = openssl_pkey_get_public(
                "-----BEGIN PUBLIC KEY-----" . PHP_EOL .
                $issuer_pubkey . PHP_EOL .
                "-----END PUBLIC KEY-----"
            );
            $jwt_valid = openssl_verify(
                substr($token, 0, strpos($token, JWT_GLUE, strpos($token, JWT_GLUE) + 1)),
                $signature,
                $pubkey,
                JWT_RS_ALGO_MAP[$issuer_algorithm]
            );
        } else if ($issuer_algorithm === JWT_ALGO_NONE) {
            $jwt_valid = true;
        } else {
            throw new InvalidArgumentException("Unsupported issuer algorithm: " . $issuer_algorithm);
        }

        if (!$jwt_valid) {
            return null;
        }

        $token_data = base64_decode($segments[1], true);
        $token_data = json_decode($token_data, true, 6);

        if (!$token_data) {
            return null;
        }

        return new self($token_data, $token, $issuer_pubkey, $issuer_algorithm);
    }

    public function isValid($audience) : bool {
        return !($this->invalidReason($audience) instanceof WP_Error);
    }

    public function invalidReason($audience): ?WP_Error
    {
        if (!isset($this->data['iat']) || !is_int($this->data['iat']) || $this->data['iat'] >= time()) {
            return new WP_Error(
                'jwt_not_yet_valid',
                __( 'Unauthorized' ),
            );
        }

        if (!isset($this->data['exp']) || !is_int($this->data['exp']) || $this->data['exp'] < time()) {
            return new WP_Error(
                'jwt_expired',
                __( 'Unauthorized' ),
            );
        }

        if (!isset($this->data['aud']) || $this->data['aud'] !== $audience) {
            return new WP_Error(
                'jwt_invalid_audience',
                __( 'Unauthorized' ),
            );
        }

        if (!isset($this->data['sub']) || !is_string($this->data['sub']) || !$this->data['sub']) {
            return new WP_Error(
                'jwt_invalid_subject',
                __( 'Unauthorized' ),
            );
        }

        return null;
    }

    public function getData() {
        // Return a copy of the object
        return unserialize(serialize($this->data));
    }

    public function getToken() : string {
        return $this->token;
    }

    public function getIssuerKey() : string {
        return $this->issuer_pubkey;
    }

    public function getIssuerAlgorithm() : string {
        return $this->issuer_algorithm;
    }
}